const ratedWidth = 600;
const ratedHeight = 400;
const svgWidth = window.innerWidth;
const svgHeight = window.innerHeight;

const draw = SVG().addTo('#draw')
      .size(svgWidth, svgHeight);

const firstQuarter = (active) => {
    draw.line(
        svgWidth / 2 - 60,
        svgHeight / 2 - 150,
        svgWidth / 2 + 60,
        svgHeight / 2 - 150
    ).stroke({ color: active === 0 ? '#000' : '#eee', width: 18 });


    draw.line(
        svgWidth / 2 + 100,
        svgHeight / 2 - 140,
        svgWidth / 2 + 210,
        svgHeight / 2 - 80
    ).stroke({ color: active === 1 ? '#000' : '#eee', width: 18 });

    draw.line(
        svgWidth / 2 + 250,
        svgHeight / 2 - 50,
        svgWidth / 2 + 250,
        svgHeight / 2 + 50
    ).stroke({ color: active === 2 ? '#000' : '#eee', width: 18 });
};

const secondQuarter = (active) => {

    draw.line(
        svgWidth / 2 + 250,
        svgHeight / 2 - 50,
        svgWidth / 2 + 250,
        svgHeight / 2 + 50
    ).stroke({ color: active === 3 ? '#000' : '#eee', width: 18 });

    draw.line(
        svgWidth / 2 + 100,
        svgHeight / 2 + 140,
        svgWidth / 2 + 210,
        svgHeight / 2 + 80
    ).stroke({ color: active === 4 ? '#000' : '#eee', width: 18 });

    draw.line(
        svgWidth / 2 - 60,
        svgHeight / 2 + 150,
        svgWidth / 2 + 60,
        svgHeight / 2 + 150
    ).stroke({ color: active === 5 ? '#000' : '#eee', width: 18 });
};

const thirdQuarter = (active) => {
    draw.line(
        svgWidth / 2 - 60,
        svgHeight / 2 + 150,
        svgWidth / 2 + 60,
        svgHeight / 2 + 150
    ).stroke({ color: active === 6 ? '#000' : '#eee', width: 18 });

    draw.line(
        svgWidth / 2 - 100,
        svgHeight / 2 + 140,
        svgWidth / 2 - 210,
        svgHeight / 2 + 80
    ).stroke({ color: active === 7 ? '#000' : '#eee', width: 18 });

    draw.line(
        svgWidth / 2 - 250,
        svgHeight / 2 - 50,
        svgWidth / 2 - 250,
        svgHeight / 2 + 50
    ).stroke({ color: active === 8 ? '#000' : '#eee', width: 18 });
};

const fourthQuarter = (active) => {
    draw.line(
        svgWidth / 2 - 250,
        svgHeight / 2 - 50,
        svgWidth / 2 - 250,
        svgHeight / 2 + 50
    ).stroke({ color: active === 9 ? '#000' : '#eee', width: 18 });

    draw.line(
        svgWidth / 2 - 100,
        svgHeight / 2 - 140,
        svgWidth / 2 - 210,
        svgHeight / 2 - 80
    ).stroke({ color: active === 10 ? '#000' : '#eee', width: 18 });

    draw.line(
        svgWidth / 2 - 60,
        svgHeight / 2 - 150,
        svgWidth / 2 + 60,
        svgHeight / 2 - 150
    ).stroke({ color: active === 11 ? '#000' : '#eee', width: 18 });
};

const drawRated = (time) => {
    // main ellipse
    draw.ellipse(ratedWidth, ratedHeight)
        .attr({fill: '#fff', cx: svgWidth / 2, cy: svgHeight / 2})
        .stroke({ color: '#000', width: 5 });

    // inner gray indicator
    draw.ellipse(ratedWidth - 20, ratedHeight - 20)
        .attr({fill: '#fff', cx: svgWidth / 2, cy: svgHeight / 2})
        .stroke({ color: '#eee', width: 20 });

    const a = ratedWidth / 2;
    const b = ratedHeight / 2;

    // round hour indicator
    let hour = time.getHours();
    if (hour > 12) {
        hour -= 12;
    } else if (hour === 0) {
        hour = 12;
    }

    // 4 sections
    [0, 90, 180, 270].forEach((section) => {
        const psi = section * Math.PI / 180;
        const fi = Math.atan2(a * Math.sin(psi), b * Math.cos(psi));
        const x = (a - 20) * Math.cos(fi) + svgWidth / 2 - 15;
        const y = (b - 20) * Math.sin(fi) + svgHeight / 2 - 8;
        draw.rect(30, 15)
            .attr({x, y, fill: '#fff'})
            .rotate(section);
    });


    const angle = hour * 30 - 90;
    const psi = angle * Math.PI / 180;
    const fi = Math.atan2(a * Math.sin(psi), b * Math.cos(psi));
    const cx = a * Math.cos(fi) + svgWidth / 2;
    const cy = b * Math.sin(fi) + svgHeight / 2;
    draw.circle(25)
        .stroke({ color: '#000', width: 2 })
        .attr({cx, cy, fill: '#eee'})
        .rotate(angle + 90);


    const minutes = time.getMinutes();
    const centerLine = draw
        .line(
            svgWidth / 2 - 120,
            svgHeight / 2,
            svgWidth / 2 + 120,
            svgHeight / 2
        )
        .stroke({ color: '#000', width: 12 })
        .x(svgWidth / 2 - 240 + minutes * 4);

    const centralRect = draw
        .rect(56, 56)
        .attr({fill: '#eee', x: svgWidth / 2 - 30, y: svgHeight / 2 - 30})
        .stroke({ color: '#000', width: 4 });

    const takeActive = (minutes) => Math.floor(minutes / 5);

    switch(Math.floor(minutes / 15)) {
        case 0:
            return firstQuarter(takeActive(minutes));
        case 1:
            return secondQuarter(takeActive(minutes));
        case 2:
            return thirdQuarter(takeActive(minutes));
        case 3:
            return fourthQuarter(takeActive(minutes));
    }
};


drawRated(new Date());
let initialInterval = setInterval(() => {
    draw.clear();
    drawRated(new Date());
}, 20000);

document.getElementById('time').addEventListener('change', (e) => {
    clearInterval(initialInterval);
    draw.clear();
    const time = new Date();
    drawRated(new Date(time.setHours.apply(time, e.target.value.split(':'))));
});
